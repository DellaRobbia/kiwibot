# CHANGELOG

Reprise du développement initialement commencé depuis ce projet : [https://gitlab.com/DellaRobbia/webhook-kiwi/-/blob/master/CHANGELOG.md](gitlab.com/DellaRobbia/webhook-kiwi)

Tous les changements notables de ce projet sont documentés ici.

Le format est basé sur [Keep a Changelog](http://keepachangelog.com/).

-   `Added` pour les nouvelles fonctionnalités.
-   `Changed` pour les changements aux fonctionnalités préexistantes.
-   `Deprecated` pour les fonctionnalités qui seront bientôt supprimées.
-   `Removed` pour les fonctionnalités désormais supprimées.
-   `Fixed` pour les corrections de bugs.
-   `Security` en cas de vulnérabilités.

## [0.0.1]
### Changed

- Initiation du projet depuis l’ancien projet (en JS) vers Python

## OLD : [0] -> [0.4.2]
Cf. `https://gitlab.com/DellaRobbia/webhook-kiwi/-/blob/master/CHANGELOG.md`
Langage : JS

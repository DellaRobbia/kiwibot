# Kiwi Bot
[![Documentation Status](https://readthedocs.org/projects/discord-kiwibot/badge/?version=latest)](https://discord-kiwibot.readthedocs.io/fr/latest/?badge=latest)

## Présentation

Kiwi Bot est une application, aussi appelé bot, pour la plateforme de discussion instantannée Discord. Cette application est codée en Python.<br>
Initialement, elle fut codée en JavaScript dans le but d’apprendre et de me familiariser avec ce langage de programmation. Le code de la version JS est toujours disponible sur la page du dépôt GitLab : [webhook-kiwi](https://gitlab.com/DellaRobbia/webhook-kiwi/-/blob/master/CHANGELOG.md).<br>
Après 2 ans de développement et n’ayant plus d’intérêt à utiliser le JS, j’ai décidé de migrer le bot vers Python, langage de programmation qui me sert toujours personnellement.
